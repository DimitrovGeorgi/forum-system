package com.example.forumsystemapplication.services;


import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.repository.repo.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.forumsystemapplication.helpers.Helpers.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {
    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void get_Should_CallRepository() {
        FilterOptionsUsers mockFilterOptions = createFilterOptionsUser();

        service.get(mockFilterOptions);

        Mockito.verify(mockRepository, times(1))
                .get(mockFilterOptions);
    }
    @Test
    public void get_Should_ReturnComment_When_MatchById() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = service.getById(mockUser.getId());

        Assertions.assertEquals(mockUser, result);
    }
    @Test
    public void get_Should_ReturnComment_When_ByUsername() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        User result = service.getByUsername(mockUser.getUsername());

        Assertions.assertEquals(mockUser, result);
    }
    @Test
    public void create_Should_CallRepository_When_NoDuplicateExists() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockUser);

        Mockito.verify(mockRepository, times(1))
                .create(mockUser);
    }
    @Test
    public void create_Should_ThrowException_When_DuplicateExists() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> service.create(mockUser));
    }
    @Test
    public void testSetUserToAdmin() {
        User mockUser = createMockUser();
        User mockAdmin = createMockUser();
        mockAdmin.setRole(Role.ADMIN);

        service.setUserToAdmin(mockUser, mockAdmin);

        Mockito.verify(mockRepository, times(1)).setUserToAdmin(mockUser);
    }
    @Test
    public void testChangeStatusToBlock() {
        User mockUser = createMockUser();
        User mockUserAuthorization = createMockUser();
        mockUserAuthorization.setRole(Role.ADMIN);

        service.changeStatusToBlock(mockUser, mockUserAuthorization);

        Mockito.verify(mockRepository, times(1)).changeStatusToBlock(mockUser);
    }
    @Test
    public void testChangeStatusToActive() {
        User mockUser = createMockUser();
        User mockAdmin = createMockUser();
        mockAdmin.setRole(Role.ADMIN);

        service.changeStatusToActive(mockUser, mockAdmin);

        Mockito.verify(mockRepository, times(1)).changeStatusToActive(mockUser);
    }
    @Test
    public void testDelete() {
        int userId = 1;
        User mockAdmin = createMockUser();
        mockAdmin.setRole(Role.ADMIN);

        service.delete(userId, mockAdmin);

        Mockito.verify(mockRepository, times(1)).delete(userId);
    }

}
