package com.example.forumsystemapplication.services;


import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;

import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.repository.repo.CommentRepository;

import com.example.forumsystemapplication.repository.repo.PostRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.forumsystemapplication.helpers.Helpers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class CommentServiceTest {

    @Mock
    CommentRepository mockRepository;
    PostRepository mockPostRepository;

    @InjectMocks
    CommentServiceImpl service;

    @Test
    public void get_Should_ReturnComment_When_MatchById() {
        Comment mockComment = createMockComment();

        Mockito.when(mockRepository.getById(mockComment.getId()))
                .thenReturn(mockComment);

        Comment result = service.getById(mockComment.getId());

        Assertions.assertEquals(mockComment, result);
    }

    @Test
    public void create_Should_CallRepository_When_CreateComment() {
        CommentRepository mockRepository = mock(CommentRepository.class);
        PostRepository mockPostRepository = mock(PostRepository.class);

        CommentServiceImpl service = new CommentServiceImpl(mockRepository, mockPostRepository);

        Comment mockComment = createMockComment();
        User mockUser = mockComment.getCreateBy();
        Post mockPost = createMockPost();


        service.create(mockComment, mockUser, mockPost);

        Mockito.verify(mockRepository, Mockito.times(1)).create(mockComment);
        Mockito.verify(mockPostRepository, Mockito.times(1)).update(mockPost);
    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        Comment mockComment = createMockComment();
        User mockUser = mockComment.getCreateBy();


        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.update(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockComment);

    }


    @Test
    void update_Should_CallRepository_When_UserIsAdmin() {
        User mockUser = createMockUser();
        mockUser.setUsername("Kiril");
        Comment mockComment = createMockComment();
        mockUser.setRole(Role.ADMIN);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.update(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockComment);
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsCreator() {
        Comment mockComment = createMockComment();
        User mockUserCreator = mockComment.getCreateBy();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.delete(1, mockUserCreator);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(1);
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsAdmin() {
        User mockUser = createMockUser();
        mockUser.setUsername("Kiril");
        Comment mockComment = createMockComment();
        mockUser.setRole(Role.ADMIN);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.delete(1, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(1);
    }


    @Test
    public void delete_Should_ThrowException_When_UserIsNotAdminOrCreator() {
        User mockUser = createMockUser();
        mockUser.setUsername("Kiril");
        Comment mockComment = createMockComment();
        mockUser.setRole(Role.USER);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        service.delete(1, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(1);;
    }
    @Test
    public void delete_Should_ThrowException_When_UserIsNotAktive() {
        User mockUser = createMockUser();
        mockUser.setUsername("Kiril");
        Comment mockComment = createMockComment();
        mockUser.setStatus(Status.BLOCK);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockComment);

        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.delete(1, mockUser));
    }

    @Test
    public void get_Should_ReturnListComments() {
        List<Comment> commentList = new ArrayList<>();

        Comment mockComment = createMockComment();
        commentList.add(mockComment);

        Mockito.when(mockRepository.get())
                .thenReturn(commentList);

        List<Comment> result = service.get();

        Assertions.assertEquals(commentList, result);
    }

    @Test
    public void getCommentsByUser_Should_ReturnCommentsForUser() {
        User mockUser = createMockUser();
        List<Comment> mockPosts = new ArrayList<>();

        Comment mockPost = createMockComment();
        mockPosts.add(mockPost);

        Mockito.when(mockRepository.getCommentsByUser(mockUser))
                .thenReturn(mockPosts);

        List<Comment> result = service.getCommentsByUser(mockUser);

        Assertions.assertEquals(mockPosts, result);
    }
    @Test
    public void getCommentsByUser_Should_CallRepository() {

        User mockUser = createMockUser();
        List<Comment> commentList = new ArrayList<>();

        Comment mockComment = createMockComment();
        commentList.add(mockComment);

        Mockito.when(mockRepository.getCommentsByUser(mockUser))
                .thenReturn(commentList);

        List<Comment> result = service.getCommentsByUser(mockUser);

        Assertions.assertEquals(commentList, result);
    }
    @Test
    public void getCommentsByUser_Should_Throw_UnauthorizedOperationException() {

        User mockUser = createMockUser();
        List<Comment> commentList = new ArrayList<>();

        Comment mockComment = createMockComment();
        commentList.add(mockComment);



        mockUser.setStatus(Status.BLOCK);
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.getCommentsByUser(mockUser));
    }
    @Test
    public void getCommentsByPost_Should_CallRepository() {

        User mockUser = createMockUser();
        List<Comment> commentList = new ArrayList<>();

        Comment mockComment = createMockComment();
        commentList.add(mockComment);
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.getCommentsByPost(mockPost))
                .thenReturn(commentList);

        List<Comment> result = service.getCommentsByPost(mockPost);

        Assertions.assertEquals(commentList, result);
    }
    @Test
    public void get_Should_CallRepository() {


        List<Comment> commentList = new ArrayList<>();

        Comment mockComment = createMockComment();
        commentList.add(mockComment);


        Mockito.when(mockRepository.get())
                .thenReturn(commentList);

        List<Comment> result = service.get();

        Assertions.assertEquals(commentList, result);
    }
    @Test
    public void getById_Should_CallRepository() {


        Comment mockComment = createMockComment();


        Mockito.when(mockRepository.getById(1))
                .thenReturn(mockComment);

        Comment result = service.getById(1);

        Assertions.assertEquals(mockComment, result);
    }
    @Test
    public void getById_Should_CallRepository_Throw_UnauthorizedOperationException() {


        Comment mockComment = createMockComment();
        mockComment.setStatus(Status.BLOCK);



        Mockito.when(mockRepository.getById(1))
                .thenReturn(mockComment);



        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.getById(1));
    }
    @Test
    public void update_Should_CallRepository_When_ModifyPermissionsAreMet() {
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        service.update(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockComment);
    }

    @Test
    public void update_Should_ThrowException_When_ModifyPermissionsAreNotMet() {
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();


        mockComment.setStatus(Status.BLOCK);

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> service.update(mockComment, mockUser));
    }

    @Test
    public void updateCommentLike_Should_CallRepository_When_LikeAdded() {
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();


        mockComment.getUserLikes().remove(mockUser);

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        service.updateCommentLike(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockComment);
    }

    @Test
    public void updateCommentLike_Should_CallRepository_When_LikeRemoved() {
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();


        mockComment.getUserLikes().add(mockUser);

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        service.updateCommentLike(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockComment);
    }

    @Test
    public void updateCommentLike_Should_CallRepository_When_LikeAddedOrRemoved() {
        Comment mockComment = createMockComment();
        User mockUser = createMockUser();

        mockComment.getUserLikes().remove(mockUser);

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockComment);

        service.updateCommentLike(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockComment);

        mockComment.getUserLikes().add(mockUser);

        Mockito.clearInvocations(mockRepository);

        service.updateCommentLike(mockComment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).update(mockComment);
    }


}
