package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;

import com.example.forumsystemapplication.repository.repo.CommentRepository;
import com.example.forumsystemapplication.repository.repo.PostRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.example.forumsystemapplication.helpers.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;


@ExtendWith(MockitoExtension.class)
public class PostServiceTests {
    @Mock
    PostRepository mockRepository;
    CommentRepository mockCommentRepository;

    @InjectMocks
    PostServiceImpl service;

    @Test
    public void get_Should_CallRepository() {
        FilterOptionsPosts mockFilterOptions = createFilterOptionsPosts();

        service.get(mockFilterOptions);

        Mockito.verify(mockRepository, times(1))
                .get(mockFilterOptions);
    }
    @Test
    public void getLast10Post_Should_ReturnLast10Posts() {
        List<Post> mockPosts = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            Post mockPost = createMockPost();
            mockPost.setId(i);
            mockPosts.add(mockPost);
        }

        Mockito.when(mockRepository.get(any(FilterOptionsPosts.class)))
                .thenReturn(mockPosts);

        List<Post> result = service.getLast10Post(new FilterOptionsPosts());


        for (int i = 0; i < 10; i++) {
            int expectedId = 20 - i;
            int actualId = result.get(i).getId();
            assertEquals(expectedId, actualId);
        }
    }
    @Test
    public void getTop10CommentedPosts_Should_ReturnTop10CommentedPosts() {
        List<Post> mockPosts = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            Post mockPost = createMockPost();
            mockPost.setId(i);
            mockPost.setStatus(Status.ACTIVE);
            mockPost.setCommentsCount(20 - i);
            mockPosts.add(mockPost);
        }

        Mockito.when(mockRepository.get(any(FilterOptionsPosts.class)))
                .thenReturn(mockPosts);

        List<Post> result = service.getTop10CommentedPosts(new FilterOptionsPosts());


        for (int i = 0; i < 10; i++) {
            int expectedId = i + 1;
            int actualId = result.get(i).getId();
            assertEquals(expectedId, actualId);
        }
    }

    @Test
    public void updatePostLike_Should_AddUserToLikeList_When_UserHasNotLiked() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        Set<User> userLikes = new HashSet<>();
        mockPost.setUserLikes(userLikes);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.updatePostLike(mockPost, mockUser);

        Assertions.assertTrue(mockPost.getUserLikes().contains(mockUser));
        Mockito.verify(mockRepository, times(1)).update(mockPost);
    }

    @Test
    public void updatePostLike_Should_RemoveUserFromLikeList_When_UserHasLiked() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        Set<User> userLikes = new HashSet<>();
        userLikes.add(mockUser);
        mockPost.setUserLikes(userLikes);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.updatePostLike(mockPost, mockUser);

        Assertions.assertFalse(mockPost.getUserLikes().contains(mockUser));
        Mockito.verify(mockRepository, times(1)).update(mockPost);
    }

    @Test
    public void get_Should_ReturnPost_When_MatchById() {
        Post mockPost = createMockPost();

        Mockito.when(mockRepository.getById(mockPost.getId()))
                .thenReturn(mockPost);

        Post result = service.getById(mockPost.getId());

        assertEquals(mockPost, result);
    }

    @Test
    public void create_Should_CallRepository_When_NoDuplicateExists() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByTitle(mockPost.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockPost, mockUser);

        Mockito.verify(mockRepository, times(1))
                .create(mockPost);
    }
    @Test
    public void create_Should_ThrowException_When_DuplicateExists() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByTitle(mockPost.getTitle()))
                .thenReturn(mockPost);

        Assertions.assertThrows(EntityDuplicateException.class,
                ()-> service.create(mockPost,mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.update(mockPost,mockUser);

        Mockito.verify(mockRepository, times(1))
                .update(mockPost);
    }


    @Test
    public void delete_Should_ThrowException_When_ModifyPermissionsAreNotMet() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();

        mockPost.setCreateBy(createMockUser()); // Different creator

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Assertions.assertThrows(
                NullPointerException.class,
                () -> service.delete(mockPost.getId(), mockUser));
    }

    @Test
    void update_Should_CallRepository_When_UserIsAdmin() {
        Post mockPost = createMockPost();
        User mockUser = createMockUser();
        mockUser.setUsername("Kiril");
        mockUser.setRole(Role.ADMIN);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        service.update(mockPost,mockUser);

        Mockito.verify(mockRepository, times(1))
                .update(mockPost);
    }

    @Test
    public void update_Should_ThrowException_When_PostIdIsTaken() {
        Post mockPost = createMockPost();
        User mockUser = mockPost.getCreateBy();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockPost);

        Post mockExistingPost = createMockPost();
        mockPost.setId(2);

        Mockito.when(mockRepository.getById(mockPost.getId()))
                .thenReturn(mockExistingPost);

        Assertions.assertThrows(EntityDuplicateException.class,
                ()-> service.update(mockPost,mockUser));


    }

    @Test
    public void update_Should_ThrowException_When_EntityNotFoundException() {
        Post mockPost = createMockPost();
        User mockUser = mockPost.getCreateBy();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class,
                ()-> service.update(mockPost,mockUser));

    }


    @Test
    public void getPostsByUser_Should_ReturnPostsForUser() {
        Post mockPost=createMockPost();
        List<Post> mockList = new ArrayList<>();
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getPostsByUser(mockUser))
                .thenReturn(mockList);

        List<Post> result = service.getPostsByUser(mockUser);

        assertEquals(mockList,result);


    }
}