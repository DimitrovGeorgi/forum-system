package com.example.forumsystemapplication.services;


import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;

import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;

import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.repository.repo.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.forumsystemapplication.helpers.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTest {
    @Mock
    TagRepository mockRepository;

    @InjectMocks
    TagServiceImpl service;

    @Test
    public void get_Should_ReturnListTags() {
        List<Tag> tags = new ArrayList<>();

        Mockito.when(mockRepository.get())
                .thenReturn(tags);

        List<Tag> result = service.get();

        Assertions.assertEquals(tags,result);


    }
    @Test
    public void get_Should_ReturnTag_When_MatchById() {
        Tag mockTag = createMockTag();

        Mockito.when(mockRepository.getById(1))
                .thenReturn(mockTag);

        Tag result = service.getById(1);

        Assertions.assertEquals(mockTag,result);
    }
    @Test
    public void create_Should_CallRepository_When_CreateTag() {
        Tag mockTag = createMockTag();
        User mockUser = mockTag.getCreateBy();
        Post mockPost = mockTag.getPostOn();

        service.create(mockTag,mockUser,mockPost);

        Mockito.verify(mockRepository,Mockito.times(1))
                .create(mockTag);



    }
    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        Tag mockTag = createMockTag();
        User mockUser = mockTag.getCreateBy();


        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockTag);

        service.update(mockTag,mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .update(mockTag);



    }
    @Test
    public void testGetTagsByUser() {
        User mockUser = createMockUser();
        List<Tag> mockTags = Arrays.asList(createMockTag(),createMockTag());

        Mockito.when(mockRepository.getTagsByUser(mockUser)).thenReturn(mockTags);

        List<Tag> result = service.getTagsByUser(mockUser);

        Assertions.assertEquals(mockTags, result);
    }
    @Test
    public void testGetTagsByPost() {
        Post mockPost = createMockPost();
        List<Tag> mockTags = Arrays.asList(createMockTag(),createMockTag());

        Mockito.when(mockRepository.getTagsByPost(mockPost)).thenReturn(mockTags);

        List<Tag> result = service.getTagsByPost(mockPost);

        Assertions.assertEquals(mockTags, result);
    }

    @Test
    void update_Should_CallRepository_When_UserIsAdmin() {
        Tag mockTag = createMockTag();
        User mockUser = mockTag.getCreateBy();
        mockUser.setRole(Role.ADMIN);

        service.update(mockTag,mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .update(mockTag);


    }
    @Test
    public void delete_Should_CallRepository_When_UserIsCreator() {
        Tag mockTag = createMockTag();
        User mockUser = mockTag.getCreateBy();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                        .thenReturn(mockTag);

        service.delete(1,mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .delete(1);


    }
    @Test
    public void delete_Should_CallRepository_When_UserIsAdmin() {
        User mockUser = createMockUser();
        mockUser.setUsername("Kiril");
        Tag mockTag = createMockTag();
        mockUser.setRole(Role.ADMIN);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockTag);

        service.delete(1,mockUser);

        Mockito.verify(mockRepository,Mockito.times(1))
                .delete(1);

    }
    
}
