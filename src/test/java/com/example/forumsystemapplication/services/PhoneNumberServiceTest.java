package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.repository.repo.CommentRepository;
import com.example.forumsystemapplication.repository.repo.PhoneNumberRepository;
import com.example.forumsystemapplication.repository.repo.PostRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class PhoneNumberServiceTest {
    @Mock
    PhoneNumberRepository mockRepository;

    @InjectMocks
    PhoneNumberServiceImpl service;

    @Test
    public void testCreatePhoneNumber() {
        PhoneNumber phoneNumber = new PhoneNumber();
        ReflectionTestUtils.setField(phoneNumber, "id", 1); // Set ID manually for testing

        service.create(phoneNumber);

        Mockito.verify(mockRepository, times(1)).create(phoneNumber);
    }

}
