package com.example.forumsystemapplication.helpers;

import com.example.forumsystemapplication.models.*;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Helpers {

    public static Admin createMockAdmin(){
        return new Admin(createMockUser(),"0888123456");
    }


    public static User createMockUser(){
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirst_name("MockFirstName");
        mockUser.setLast_name("MockLastName");
        mockUser.setEmail("Mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setRole(Role.USER);
        mockUser.setStatus(Status.ACTIVE);

        return  mockUser;
    }

    public static  Post createMockPost(){
        Post mockPost = new Post();
        mockPost.setId(1);
        mockPost.setCreateBy(createMockUser());
        mockPost.setTitle("MockTitle");
        mockPost.setContent("MockContent");
        mockPost.setStatus(Status.ACTIVE);
        return mockPost;
    }

    public static  Comment createMockComment(){
        Comment mockComment = new Comment();
        mockComment.setId(1);
        mockComment.setCreateBy(createMockUser());
        mockComment.setPostOn(createMockPost());
        mockComment.setContent("Great guide, thanks for sharing!");
        mockComment.setStatus(Status.ACTIVE);
        return mockComment;
    }
    public static Tag createMockTag(){
        Tag mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName("mercedes");
        mockTag.setCreateBy(createMockUser());
        mockTag.setPostOn(createMockPost());
        return mockTag;
    }
    public static FilterOptionsPosts createFilterOptionsPosts(){
        return new FilterOptionsPosts(
                "title",
                "content",
                "sort",
                "order");
    }

    public static FilterOptionsUsers createFilterOptionsUser(){
        return  new FilterOptionsUsers(
                "first_name",
                "last_name",
                "username",
                "email",
                "sort",
                "order");
    }

    public  static  String toJson(final Object obj){
        try{
            return  new ObjectMapper().writeValueAsString(obj);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
