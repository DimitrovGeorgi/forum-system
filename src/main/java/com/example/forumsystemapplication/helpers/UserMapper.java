package com.example.forumsystemapplication.helpers;

import com.example.forumsystemapplication.models.Dto.PhoneNumberDto;
import com.example.forumsystemapplication.models.Dto.PostDto;
import com.example.forumsystemapplication.models.Dto.RegisterDto;
import com.example.forumsystemapplication.models.Dto.UpdateUserDto;
import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.repository.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final UserRepository userRepository;

    @Autowired
    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public User fromDto(int id, UpdateUserDto dto) {
        User user = userRepository.getById(id);
        user.setPassword(dto.getChangePassword());
        return user;
    }

    public User fromDto(RegisterDto dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setFirst_name(dto.getFirstName());
        user.setLast_name(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setRole(Role.USER);
        user.setStatus(Status.ACTIVE);
        user.setPhotoUrl(dto.getPhotoUrl());
        return user;
    }

    public UpdateUserDto toDto(User user) {
        UpdateUserDto dto = new UpdateUserDto();
        dto.setPassword(user.getPassword());
        return dto;
    }


}
