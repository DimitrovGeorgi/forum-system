package com.example.forumsystemapplication.helpers;

import com.example.forumsystemapplication.models.Dto.CommentDto;
import com.example.forumsystemapplication.models.Dto.TagDto;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.TagService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TagMapper {

    private final TagService tagService;

    @Autowired
    public TagMapper(TagService tagService) {
        this.tagService = tagService;
    }

    public Tag fromDto(int id, TagDto tagDto) {
        Tag tag = fromDto(tagDto);
        tag.setId(id);
        Tag tagRepo = tagService.getById(id);
        tag.setCreateBy(tagRepo.getCreateBy());
        tag.setPostOn(tagRepo.getPostOn());
        return tag;
    }

    public Tag fromDto(TagDto dto) {
        Tag tag = new Tag();
        tag.setName(dto.getName());
        return tag;
    }


    public TagDto toDto(Tag tag) {
        TagDto tagDto = new TagDto();
        tagDto.setName(tag.getName());
        tagDto.setPostId(tag.getPostOn().getId());
        return tagDto;
    }

    public List<TagDto> commentDtoList(List<Tag> tags) {
        List<TagDto> tagDtos = new ArrayList<>();
        for (Tag tag : tags) {
            tagDtos.add(toDto(tag));
        }
        return tagDtos;
    }

}
