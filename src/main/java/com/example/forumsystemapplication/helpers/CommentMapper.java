package com.example.forumsystemapplication.helpers;

import com.example.forumsystemapplication.models.Comment;

import com.example.forumsystemapplication.models.Dto.CommentDto;
import com.example.forumsystemapplication.services.service.CommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentMapper {

    private final CommentService commentService;


    @Autowired
    public CommentMapper(CommentService commentService) {
        this.commentService = commentService;
    }

    public Comment fromDto(int id, CommentDto dto) {
        Comment comment = fromDto(dto);
        comment.setId(id);
        Comment commentRepo = commentService.getById(id);
        comment.setCreateBy(commentRepo.getCreateBy());
        comment.setPostOn(commentRepo.getPostOn());
        return comment;
    }

    public Comment fromDto(CommentDto commentDto) {
        Comment comment = new Comment();
        comment.setContent(commentDto.getContent());
        return comment;
    }


    public CommentDto toDto(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setContent(comment.getContent());
        commentDto.setPostId(comment.getPostOn().getId());
        return commentDto;
    }

    public List<CommentDto> commentDtoList(List<Comment> comments) {
        List<CommentDto> commentDtos = new ArrayList<>();
        for (Comment comment : comments) {
            commentDtos.add(toDto(comment));
        }
        return commentDtos;
    }

}
