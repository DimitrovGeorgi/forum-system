package com.example.forumsystemapplication.helpers;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

import static com.example.forumsystemapplication.utils.constants.*;

@Component
public class AuthenticationHelper {

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "The request resource required authorization. ");
        }
        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username");
        }
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            if(user.getStatus().toString().equals("Block")){
                throw new AuthenticationFailureException(USER_BLOCK);
            }
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(THIS_USERNAME_DOES_NOT_EXISTS);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(THIS_USERNAME_DOES_NOT_EXISTS);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");
        if (currentUser == null) {
            throw new AuthenticationFailureException("No logged User");
        }
        return userService.getByUsername(currentUser);
    }

}

