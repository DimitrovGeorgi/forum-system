package com.example.forumsystemapplication.helpers;

import com.example.forumsystemapplication.models.Dto.PhoneNumberDto;
import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.repository.repo.UserRepository;
import com.example.forumsystemapplication.services.service.PhoneNumberService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class PhoneNumberMapper {

    private final UserService userService;

    private final PhoneNumberService phoneNumberService;

    public PhoneNumberMapper(UserService userService, PhoneNumberService phoneNumberService) {
        this.userService = userService;
        this.phoneNumberService = phoneNumberService;
    }

    public PhoneNumber from (PhoneNumberDto dto){
        PhoneNumber phoneNumber = new PhoneNumber();

        phoneNumber.setPhone_number(dto.getPhoneNumber());

        return phoneNumber;
    }
}
