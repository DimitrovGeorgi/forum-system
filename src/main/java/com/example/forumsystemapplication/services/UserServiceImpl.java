package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.repository.repo.UserRepository;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.forumsystemapplication.utils.constants.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> get(FilterOptionsUsers filterOptionsUsers) {
        return userRepository.get(filterOptionsUsers);
    }
    @Override
    public List<User> getActiveUsers(FilterOptionsUsers filterOptionsUsers){
        List<User> users = userRepository.get(filterOptionsUsers);
        users = users.stream()
                .filter(post -> post.getStatus() == Status.ACTIVE)
                .collect(Collectors.toList());
        return users;
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void setUserToAdmin(User user, User admin) {
        checkStatusIsAdmin(admin);
        userRepository.setUserToAdmin(user);
    }

    @Override
    public void changeStatusToBlock(User user, User userAuthorization) {
        checkAdminOrCreator(user,userAuthorization);
        userRepository.changeStatusToBlock(user);
    }

    @Override
    public void changeStatusToActive(User user, User admin) {
        checkStatusIsAdmin(admin);
        userRepository.changeStatusToActive(user);
    }

    @Override
    public void create(User user) {
        boolean duplicateExits = true;
        try {
            userRepository.getById(user.getId());
        } catch (EntityNotFoundException e) {
            duplicateExits = false;
        }
        if (duplicateExits) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }
        userRepository.create(user);
    }

    @Override
    public void update(User user, User userAuthorization, int id) {
        checkAdminOrCreator(user,userAuthorization);
        userRepository.update(user);
    }

    @Override
    public void delete(int id,User user) {
       checkStatusIsAdmin(user);
        userRepository.delete(id);
    }


    private void checkStatusIsAdmin(User admin) {
        if (!admin.getRole().toString().equals("Admin"))
            throw new UnauthorizedOperationException(ONLY_ADMIN);
    }

    private void checkAdminOrCreator(User user, User userAuthorization){
        if (!(userAuthorization.getRole().toString().equals("Admin") || userAuthorization.getUsername().equals(user.getUsername()))) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CREATORS_CAN_MODIFY);
        }
    }

}
