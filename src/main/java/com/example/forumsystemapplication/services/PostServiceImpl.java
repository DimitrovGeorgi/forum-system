package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.models.User;

import com.example.forumsystemapplication.repository.repo.CommentRepository;
import com.example.forumsystemapplication.services.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.forumsystemapplication.repository.repo.PostRepository;

import java.util.*;
import java.util.stream.Collectors;

import static com.example.forumsystemapplication.utils.constants.*;


@Service
public class PostServiceImpl implements PostService {


    private final PostRepository postRepository;

    private final CommentRepository commentRepository;


    @Autowired
    public PostServiceImpl(PostRepository postRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Post> get(FilterOptionsPosts filterOptions) {
        List<Post> posts = postRepository.get(filterOptions);
        posts = posts.stream()
                .filter(post -> post.getStatus() == Status.ACTIVE)
                .collect(Collectors.toList());
        return posts;
    }

    @Override
    public List<Post> getLast10Post(FilterOptionsPosts filterOptions) {
        List<Post> posts = postRepository.get(filterOptions);
        posts = posts.stream()
                .filter(post -> post.getStatus() == Status.ACTIVE)
                .collect(Collectors.toList());
        Collections.reverse(posts);
        return posts;
    }

    @Override
    public List<Post> getTop10CommentedPosts(FilterOptionsPosts filterOptionsPosts) {
        List<Post> posts = postRepository.get(filterOptionsPosts);
        posts = posts.stream()
                .filter(post -> post.getStatus() == Status.ACTIVE)
                .collect(Collectors.toList());

        posts.sort(Comparator.comparingInt(Post::getCommentsCount).reversed());
        return posts.stream().limit(10).collect(Collectors.toList());
    }

    @Override
    public Post getById(int id) {
        checkStatusPost(id);
        return postRepository.getById(id);
    }

    @Override
    public List<Post> getPostsByUser(User user) {
        return postRepository.getPostsByUser(user);
    }

    @Override
    public void updatePostLike(Post post, User user) {
        Post postUpdate = postRepository.getById(post.getId());
        checkStatusPost(post.getId());
        Set<User> likeList = postUpdate.getUserLikes();

        if (!likeList.contains(user)) {
            likeList.add(user);
        } else {
            likeList.remove(user);
        }
        post.setUserLikes(likeList);
        postRepository.update(post);
    }

    @Override
    public void create(Post post, User user) {
        checkStatusUser(user);
        boolean duplicateExists = true;
        try {
            postRepository.getByTitle(post.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Post", "title", post.getTitle());
        }
        post.setCreateBy(user);
        postRepository.create(post);
    }

    @Override
    public void update(Post post, User user) {
        checkStatusPost(post.getId());
        checkModifyPermissions(post.getId(), user);
        boolean duplicateExists = true;
        try {
            Post existingPost = postRepository.getById(post.getId());
            if (existingPost.getId() == post.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new EntityDuplicateException("Post", "title", post.getTitle());
        }
        postRepository.update(post);
    }

    @Override
    public void delete(int id, User user) {
        checkModifyPermissions(id, user);
        Post post = postRepository.getById(id);
        List<Comment> comments = commentRepository.getCommentsByPost(post);
        for (int i = 0; i < comments.size(); i++) {
            comments.get(i).setStatus(Status.BLOCK);
            commentRepository.update(comments.get(i));
        }
        post.setStatus(Status.BLOCK);
        postRepository.updateWhenDelete(post);
    }

    private void checkModifyPermissions(int postId, User user) {
        Post post = postRepository.getById(postId);
        if (!(user.getRole().toString().equals("Admin") ||
                post.getCreateBy().equals(user) ||
                user.getStatus().toString().equals("Active"))) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CREATORS_CAN_MODIFY);
        }
    }

    private void checkStatusUser(User user) {
        if (!user.getStatus().toString().equals("Active"))
            throw new UnauthorizedOperationException(ONLY_FOR_ACTIVE_USERS);
    }

    private void checkStatusPost(int id) {
        if (postRepository.getById(id).getStatus().equals(Status.BLOCK)) {
            throw new UnauthorizedOperationException(POST_BLOCK);
        }
    }

}
