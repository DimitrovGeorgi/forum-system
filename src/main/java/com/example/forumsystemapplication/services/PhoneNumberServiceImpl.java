package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.repository.repo.PhoneNumberRepository;
import com.example.forumsystemapplication.services.service.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService {

    private final PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public PhoneNumberServiceImpl(PhoneNumberRepository phoneNumberRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public void create(PhoneNumber phoneNumber) {
        phoneNumberRepository.create(phoneNumber);
    }

}
