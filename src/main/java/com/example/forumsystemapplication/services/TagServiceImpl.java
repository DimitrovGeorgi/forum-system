package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.repository.repo.TagRepository;
import com.example.forumsystemapplication.services.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.forumsystemapplication.utils.constants.ONLY_ADMINS_OR_CREATORS_CAN_MODIFY;
import static com.example.forumsystemapplication.utils.constants.ONLY_FOR_ACTIVE_USERS;

@Service
public class TagServiceImpl implements TagService {


    private final TagRepository tagRepository;


    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> get() {
        return tagRepository.get();
    }

    @Override
    public Tag getById(int id) {
        return tagRepository.getById(id);
    }

    @Override
    public List<Tag> getTagsByUser(User user) {
        return tagRepository.getTagsByUser(user);
    }

    @Override
    public List<Tag> getTagsByPost(Post post) {
        return tagRepository.getTagsByPost(post);
    }

    @Override
    public void create(Tag tag, User user, Post post) {
        checkStatus(user);
        tag.setCreateBy(user);
        tag.setPostOn(post);
        tagRepository.create(tag);

    }

    @Override
    public void update(Tag tag, User user) {
        checkModifyPermissions(tag.getId(), user);
        tagRepository.update(tag);
    }

    @Override
    public void delete(int id, User user) {
        checkModifyPermissions(id, user);
        tagRepository.delete(id);
    }

    private void checkModifyPermissions(int tagId, User user) {
        Tag tag = tagRepository.getById(tagId);
        if (!(user.getRole().toString().equals("Admin") ||
                tag.getCreateBy().equals(user) ||
                user.getStatus().toString().equals("Active"))) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CREATORS_CAN_MODIFY);
        }
    }

    private void checkStatus(User user) {
        if (user.getStatus().toString().equals("Block"))
            throw new UnauthorizedOperationException(ONLY_FOR_ACTIVE_USERS);
    }

}
