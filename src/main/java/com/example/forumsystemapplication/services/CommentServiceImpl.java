package com.example.forumsystemapplication.services;

import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.repository.repo.CommentRepository;
import com.example.forumsystemapplication.repository.repo.PostRepository;
import com.example.forumsystemapplication.services.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static com.example.forumsystemapplication.utils.constants.*;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private  final PostRepository postRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    @Override
    public List<Comment> get() {
        return commentRepository.get();
    }

    @Override
    public Comment getById(int id) {
        checkStatusComment(id);
        return commentRepository.getById(id);
    }

    @Override
    public List<Comment> getCommentsByUser(User user) {
        checkStatusUser(user);
        return commentRepository.getCommentsByUser(user);
    }

    @Override
    public List<Comment> getCommentsByPost(Post post) {
        return commentRepository.getCommentsByPost(post);

    }

    @Override
    public void updateCommentLike(Comment comment,User user){
        checkStatusComment(comment.getId());
        Comment commentToUpdate= commentRepository.getById(comment.getId());
        Set<User> likeList = commentToUpdate.getUserLikes();

        if(!likeList.contains(user)){
            likeList.add(user);
        }else{
            likeList.remove(user);
        }
        comment.setUserLikes(likeList);
        commentRepository.update(comment);
    }

    @Override
    public void create(Comment comment, User user, Post post) {
        checkStatusUser(user);
        comment.setCreateBy(user);
        comment.setPostOn(post);
        post.setCommentsCount(post.getCommentsCount() + 1);
        postRepository.update(post);
        commentRepository.create(comment);
    }

    @Override
    public void update(Comment comment, User user) {
        checkStatusComment(comment.getId());
        checkModifyPermissions(comment.getId(), user);
        comment.setStatus(Status.ACTIVE);
        commentRepository.update(comment);
    }

    @Override
    public void delete(int id, User user) {
        checkStatusComment(id);
        checkModifyPermissions(id, user);
        Comment comment = commentRepository.getById(id);
        Post post = comment.getPostOn();
        if(post.getCommentsCount()>0) {
            post.setCommentsCount(post.getCommentsCount() - 1);
            postRepository.update(post);
        }
        commentRepository.delete(id);
    }

    private void checkModifyPermissions(int commentId, User user) {
        Comment comment = commentRepository.getById(commentId);
        if (!(user.getRole().toString().equals("Admin") ||
                comment.getCreateBy().equals(user) ||
                user.getStatus().toString().equals("Active"))) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CREATORS_CAN_MODIFY);
        }
    }

    private void checkStatusUser(User user) {
        if (user.getStatus().toString().equals("Block"))
            throw new UnauthorizedOperationException(ONLY_FOR_ACTIVE_USERS);
    }

    private void checkStatusComment(int id){
        if (commentRepository.getById(id).getStatus().equals(Status.BLOCK)) {
            throw  new UnauthorizedOperationException(COMMENT_BLOCK);
        }
    }

}

