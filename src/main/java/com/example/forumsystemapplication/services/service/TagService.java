package com.example.forumsystemapplication.services.service;

import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.models.User;

import java.util.List;

public interface TagService {

    List<Tag> get();

    Tag getById(int id);

    List<Tag> getTagsByUser(User user);

    List<Tag> getTagsByPost(Post post);

    void create(Tag tag, User user, Post post);

    void update(Tag tag, User user);

    void delete(int id, User user);
}
