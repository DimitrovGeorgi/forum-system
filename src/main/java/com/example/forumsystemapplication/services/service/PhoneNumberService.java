package com.example.forumsystemapplication.services.service;

import com.example.forumsystemapplication.models.PhoneNumber;

public interface PhoneNumberService {

    void create(PhoneNumber phoneNumber);
}
