package com.example.forumsystemapplication.services.service;

import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;

import java.util.List;

public interface UserService {

    List<User> get(FilterOptionsUsers filterOptionsUsers);
    public List<User> getActiveUsers(FilterOptionsUsers filterOptionsUsers);

    User getById(int id);

    void create(User user);

    void update(User user, User userAuthorization, int id);

    void delete(int id,User user);

    User getByUsername(String username);

    void setUserToAdmin(User user, User admin);

    void changeStatusToBlock(User user, User admin);

    void changeStatusToActive(User user, User admin);

}
