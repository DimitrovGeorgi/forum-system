package com.example.forumsystemapplication.services.service;

import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;

import java.util.List;

public interface PostService {

    List<Post> get(FilterOptionsPosts filterOptions);

    List<Post> getLast10Post(FilterOptionsPosts filterOptions);


    List<Post> getTop10CommentedPosts(FilterOptionsPosts filterOptionsPosts);

    Post getById(int id);

    void create(Post post, User user);

    void update(Post post, User user);

    void delete(int id, User user);

    List<Post> getPostsByUser(User user);

    void updatePostLike(Post post, User user);

}
