package com.example.forumsystemapplication.services.service;

import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;

import java.util.List;

public interface CommentService {

    List<Comment> get();

    Comment getById(int id);

    void updateCommentLike(Comment comment, User user);

    void create(Comment comment, User user, Post post);

    void update(Comment comment, User user);

    void delete(int id, User user);

    List<Comment> getCommentsByUser(User user);

    List<Comment> getCommentsByPost(Post post);

}
