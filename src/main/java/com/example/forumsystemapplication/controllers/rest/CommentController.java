package com.example.forumsystemapplication.controllers.rest;

import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.CommentMapper;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Dto.CommentDto;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.services.service.CommentService;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PostService postService;
    private final UserService userService;

    @Autowired
    public CommentController(CommentService commentService, CommentMapper commentMapper, AuthenticationHelper authenticationHelper, PostService postService, UserService userService) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
        this.postService = postService;
        this.userService = userService;
    }

    @GetMapping
    public List<CommentDto> get() {
        List<Comment> comments = commentService.get();
        return commentMapper.commentDtoList(comments);
    }

    @GetMapping("/{id}")
    public CommentDto get(@PathVariable int id) {
        try {
            Comment comment = commentService.getById(id);
            return commentMapper.toDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/user/{userId}")
    public List<CommentDto> getCommentsByUser(@PathVariable int userId) {
        try {
            User user = userService.getById(userId);
            List<Comment> comments = commentService.getCommentsByUser(user);
            return commentMapper.commentDtoList(comments);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/likes")
    public List<User> getLikesByUser(@PathVariable int id) {
        return new ArrayList<>(commentService.getById(id).getUserLikes());
    }

    @PostMapping("/{id}/likes")
    public void updateCommentLike(@PathVariable int id,@RequestHeader HttpHeaders headers) {
        try {
            Comment comment = commentService.getById(id);
            User user = authenticationHelper.tryGetUser(headers);
            commentService.updateCommentLike(comment,user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }



    @PostMapping
    public CommentDto create(@RequestHeader HttpHeaders headers, @RequestBody CommentDto commentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getById(commentDto.getPostId());
            Comment comment = commentMapper.fromDto(commentDto);
            commentService.create(comment, user, post);
            return commentMapper.toDto(comment);
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public CommentDto uprate(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody CommentDto commentDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.fromDto(id, commentDto);
            commentService.update(comment, user);
            return commentMapper.toDto(comment);
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            commentService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
