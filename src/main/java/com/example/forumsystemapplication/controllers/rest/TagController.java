package com.example.forumsystemapplication.controllers.rest;

import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.exeptions.UnauthorizedOperationException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.TagMapper;
import com.example.forumsystemapplication.models.Dto.TagDto;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagController {

    private final TagService tagService;

    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PostService postService;

    @Autowired
    public TagController(TagService tagService, TagMapper tagMapper, AuthenticationHelper authenticationHelper, PostService postService) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
        this.postService = postService;
    }

    @GetMapping
    public List<TagDto> get() {
        List<Tag> tags = tagService.get();
        return tagMapper.commentDtoList(tags);
    }

    @GetMapping("/{id}")
    public TagDto getById(@PathVariable int id) {
        try {
            Tag tag = tagService.getById(id);
            return tagMapper.toDto(tag);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public TagDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TagDto tagDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getById(tagDto.getPostId());
            Tag tag = tagMapper.fromDto(tagDto);
            tagService.create(tag, user, post);
            return tagMapper.toDto(tag);
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public TagDto update(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody TagDto tagDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Tag tag = tagMapper.fromDto(id, tagDto);
            tagService.update(tag, user);
            return tagMapper.toDto(tag);
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            tagService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
