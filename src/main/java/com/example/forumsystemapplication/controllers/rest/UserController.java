package com.example.forumsystemapplication.controllers.rest;

import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.repository.repo.UserRepository;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> get(@RequestParam(required = false) String username,
                          @RequestParam(required = false) String first_name,
                          @RequestParam(required = false) String last_name,
                          @RequestParam(required = false) String email,
                          @RequestParam(required = false) String sortBy,
                          @RequestParam(required = false) String sortOrder) {
        FilterOptionsUsers filterOptionsUsers = new FilterOptionsUsers(first_name, last_name, username, email, sortBy, sortOrder);
        List<User> users = userService.get(filterOptionsUsers);
        return users;
    }


    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody User user) {
        try {
            userService.create(user);
            return user;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody User user) {
        try {
            User userAuthorization = authenticationHelper.tryGetUser(headers);
            userService.update(user, userAuthorization, id);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/admin/{id}")
    public void changeRoleToAdmin(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userToAdmin = userService.getById(id);
            User userAuthorization = authenticationHelper.tryGetUser(headers);
            userService.setUserToAdmin(userToAdmin, userAuthorization);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/active/{id}")
    public void setActive(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User useActive = userService.getById(id);
            User userAuthorization = authenticationHelper.tryGetUser(headers);
            userService.changeStatusToActive(useActive, userAuthorization);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/block/{id}")
    public void setBlock(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User useActive = userService.getById(id);
            User userAuthorization = authenticationHelper.tryGetUser(headers);
            userService.changeStatusToBlock(useActive, userAuthorization);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id,@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id,user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
