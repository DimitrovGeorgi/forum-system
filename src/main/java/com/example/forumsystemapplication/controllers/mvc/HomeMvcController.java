package com.example.forumsystemapplication.controllers.mvc;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final PostService postService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(PostService postService, UserService userService, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            if (currentUser.getRole().equals(Role.ADMIN)) {
                return true;
            } else
                return false;
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("isBlock")
    public boolean populateIsBlock(HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            if (currentUser.getStatus().equals(Status.BLOCK)) {
                return true;
            } else
                return false;
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @GetMapping
    public String HomePage(Model model, HttpSession session) {
        List<Post> LastPosts = postService.getLast10Post(new FilterOptionsPosts());
        model.addAttribute("lastPosts", LastPosts);

        List<Post> top10 = postService.getTop10CommentedPosts(new FilterOptionsPosts());
        model.addAttribute("top10", top10);

        List<User> users = userService.getActiveUsers(new FilterOptionsUsers());
        model.addAttribute("users", users);

        List<Post> posts = postService.get(new FilterOptionsPosts());
        model.addAttribute("posts", posts);
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("logUser", logUser);
        } catch (AuthenticationFailureException e) {
            return "HomeView";
        }

        return "HomeView";


    }


}
