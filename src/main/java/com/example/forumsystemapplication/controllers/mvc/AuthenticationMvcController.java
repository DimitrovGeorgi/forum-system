package com.example.forumsystemapplication.controllers.mvc;
import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.UserMapper;
import com.example.forumsystemapplication.models.Dto.LoginDto;
import com.example.forumsystemapplication.models.Dto.RegisterDto;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final UserService userService;

    public AuthenticationMvcController(AuthenticationHelper authenticationHelper, UserMapper userMapper, UserService service) {
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.userService = service;
    }

    @ModelAttribute("isBlock")
    public boolean populateIsBlock(HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            if (currentUser.getStatus().equals(Status.BLOCK)) {
                return true;
            } else
                return false;
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "Login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto dto,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "Login";
        }

        try {
            authenticationHelper.verifyAuthentication(dto.getUsername(), dto.getPassword());
            session.setAttribute("currentUser", dto.getUsername());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "Auth_error", e.getMessage());
            return "Login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "Register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password and PasswordConfirm NOT match ");
            return "Register";
        }

        try {
            User user = userMapper.fromDto(register);
            userService.create(user);
            return "redirect:/";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "username-error", e.getMessage());
            return "Register";
        }
    }


}
