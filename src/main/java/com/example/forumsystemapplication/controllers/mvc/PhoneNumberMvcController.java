package com.example.forumsystemapplication.controllers.mvc;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.PhoneNumberMapper;
import com.example.forumsystemapplication.models.Dto.PhoneNumberDto;
import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.services.service.PhoneNumberService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/phoneNumber")
public class PhoneNumberMvcController {

    private final PhoneNumberService phoneNumberService;

    private final UserService userService;

    private final AuthenticationHelper authenticationHelper;

    private final PhoneNumberMapper phoneNumberMapper;

    public PhoneNumberMvcController(PhoneNumberService phoneNumberService, UserService userService, AuthenticationHelper authenticationHelper, PhoneNumberMapper phoneNumberMapper) {
        this.phoneNumberService = phoneNumberService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.phoneNumberMapper = phoneNumberMapper;
    }

    @GetMapping("/{id}/add")
    public String showAddPhoneNumber(@PathVariable int id, HttpSession session, Model model) {

        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin"))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User currentUser = userService.getById(id);
            model.addAttribute("phoneNumber", new PhoneNumberDto());
            model.addAttribute("user", currentUser);
            return "AddPhoneNumber";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/add")
    public String AddPhoneNumber(@PathVariable int id,
                                 @Valid @ModelAttribute("phoneNumber") PhoneNumberDto phoneNumberDto,
                                 BindingResult bindingResult,
                                 HttpSession session,
                                 Model model) {

        User logUser ;

        try {
            logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().equals(Role.ADMIN))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "AddPhoneNumber";
        }

        try {
            PhoneNumber phoneNumber = phoneNumberMapper.from(phoneNumberDto);
            phoneNumber.setUser(logUser);
            phoneNumberService.create(phoneNumber);
            return "redirect:/users/" + id;
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "UpdateUserView";
        }
    }



}
