package com.example.forumsystemapplication.controllers.mvc;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.CommentMapper;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Dto.CommentDto;
import com.example.forumsystemapplication.models.Dto.PostDto;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.services.service.CommentService;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/comments")
public class CommentMvcController {

    private final CommentService commentService;
    private final PostService postService;
    private final UserService userService;
    private final CommentMapper commentMapper;
    private final AuthenticationHelper authenticationHelper;

    public CommentMvcController(CommentService commentService, PostService postService, UserService userService, CommentMapper commentMapper, AuthenticationHelper authenticationHelper) {
        this.commentService = commentService;
        this.postService = postService;
        this.userService = userService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("users")
    public List<User> populateCommentUsers() {
        return userService.get(new FilterOptionsUsers());
    }

    @ModelAttribute("posts")
    public List<Post> populateCommentPosts() {
        return postService.get(new FilterOptionsPosts());
    }


    @GetMapping
    public String showComments(Model model) {
        model.addAttribute("comments", commentService.get());
        return "AllCommentsView";
    }

    @GetMapping("/myComments")
    public String showUserComments( Model model, HttpSession session){
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        User user = authenticationHelper.tryGetUser(session);
        List<Comment> comments = commentService.getCommentsByUser(user);
        model.addAttribute("comments",comments);
        return "MyComments";
    }

    @GetMapping("/{id}")
    public String showSingleComment(@PathVariable int id, Model model,HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Comment comment = commentService.getById(id);
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("comment", comment);
            model.addAttribute("user", user);
            return "CommentView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @PostMapping("/{id}/like")
    public String UpdateLikePost(@PathVariable int id,Model model,HttpSession session){
        try {
            Comment comment = commentService.getById(id);
            User user = authenticationHelper.tryGetUser(session);
            commentService.updateCommentLike(comment,user);
            return "redirect:/comments/" + id;
        } catch (EntityNotFoundException e){
            model.addAttribute("error",e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showCreatePage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("comment", new CommentDto());
        return "CreateCommentView";
    }

    @PostMapping("/new")
    public String createComment(@Valid @ModelAttribute("comment") CommentDto commentDto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "CreateCommentView";
        }
        try {
            Comment comment = commentMapper.fromDto(commentDto);
            Post post = postService.getById(commentDto.getPostId());
            commentService.create(comment, user, post);
            return "redirect:/posts/" + commentDto.getPostId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("content", "duplicate_comments", e.getMessage());
            return "CreatePostView";
        }
    }

    @GetMapping("/{id}/newOnPost")
    public String showCreateNewOnPostPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        Post post = postService.getById(id);
        model.addAttribute("post",post);
        model.addAttribute("comment", new CommentDto());
        model.addAttribute("id",id);
        return "CreateCommentOnPost";
    }

    @PostMapping("/{id}/newOnPost")
    public String createCommentOnPost( @PathVariable int  id,
                                @Valid @ModelAttribute("comment") CommentDto commentDto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Comment comment = commentMapper.fromDto(commentDto);
            Post post = postService.getById(id);
            commentService.create(comment, user, post);
            return "redirect:/posts/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("content", "duplicate_comments", e.getMessage());
            return "CreatePostView";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdatePage(@PathVariable int id, Model model, HttpSession session) {
        Comment comment;
        try{
            comment = commentService.getById(id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(comment.getCreateBy().getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        comment = commentService.getById(id);
        CommentDto commentDto = commentMapper.toDto(comment);
        model.addAttribute("comment", commentDto);
        return "UpdateCommentView";

    }

    @PostMapping("{id}/update")
    public String updateComment(@PathVariable int id,
                                @Valid @ModelAttribute("comment") CommentDto commentDto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {
        Comment comment = commentMapper.fromDto(id, commentDto);

        User logUser = authenticationHelper.tryGetUser(session);
        try {
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(comment.getCreateBy().getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "UpdateCommentView";
        }

        try {
            commentService.update(comment,logUser);
            return "redirect:/comments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_post", e.getMessage());
            return "UpdateCommentView";
        }
    }

    @GetMapping("{id}/delete")
    public String deleteComment(@PathVariable int id, Model model, HttpSession session) {
        Comment comment = commentService.getById(id);

        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(comment.getCreateBy().getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            commentService.delete(id, authenticationHelper.tryGetUser(session));
            return "redirect:/comments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


}
