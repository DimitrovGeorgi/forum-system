package com.example.forumsystemapplication.controllers.mvc;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.TagMapper;
import com.example.forumsystemapplication.models.Dto.TagDto;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.TagService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/tags")
public class TagMvcController {

    private final TagService tagService;
    private final PostService postService;
    private final UserService userService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;

    public TagMvcController(TagService tagService, PostService postService, UserService userService, TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.postService = postService;
        this.userService = userService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }
    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("users")
    public List<User> populateTagUsers() {
        return userService.get(new FilterOptionsUsers());
    }

    @ModelAttribute("posts")
    public List<Post> populateTagPosts() {
        return postService.get(new FilterOptionsPosts());
    }

    @GetMapping
    public String showAllTags(Model model) {
        model.addAttribute("tags", tagService.get());
        return "AllTagsView";
    }

    @GetMapping("/{id}")
    public String showSingleTag(@PathVariable int id, Model model) {
        try {
            Tag tag = tagService.getById(id);
            model.addAttribute("tag", tag);
            return "TagView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showCreateTagPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("tag", new TagDto());
        return "CreateTagView";
    }

    @PostMapping("/new")
    public String createTag(@Valid @ModelAttribute("tag") TagDto tagDto,
                            BindingResult bindingResult,
                            Model model,
                            HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "CreateTagView";
        }
        try {
            Tag tag = tagMapper.fromDto(tagDto);
            Post post = postService.getById(tagDto.getPostId());
            tagService.create(tag, user, post);
            return "redirect:/posts/" + tagDto.getPostId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("name", "duplicate-Tag", e.getMessage());
            return "CreateTagView";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdatePage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Tag tag = tagService.getById(id);
            TagDto tagDto = tagMapper.toDto(tag);
            model.addAttribute("tag", tagDto);
            return "UpdateTagView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/update")
    public String updateTag(@PathVariable int id,
                            @Valid @ModelAttribute("tag") TagDto tagDto,
                            BindingResult bindingResult,
                            Model model,
                            HttpSession session) {
        User currentUser = userService.getById(id);
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(currentUser.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "UpdateTagView";
        }
        try {
            Tag tag = tagMapper.fromDto(id, tagDto);
            tagService.update(tag, currentUser);
            return "redirect:/posts/" + tagDto.getPostId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("name", "duplicate-Tag Name", e.getMessage());
            return "UpdateTagView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteTag(@PathVariable int id, Model model, HttpSession session) {
        User currentUser = userService.getById(id);
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(currentUser.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            tagService.delete(id, currentUser);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

}
