package com.example.forumsystemapplication.controllers.mvc;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.PhoneNumberMapper;
import com.example.forumsystemapplication.helpers.UserMapper;
import com.example.forumsystemapplication.models.Dto.PhoneNumberDto;
import com.example.forumsystemapplication.models.Dto.UpdateUserDto;
import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.models.filters.FilterUserDto;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    private final PhoneNumberMapper phoneNumberMapper;

    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, PhoneNumberMapper phoneNumberMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.phoneNumberMapper = phoneNumberMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            if (currentUser.getRole().equals(Role.ADMIN)) {
                return true;
            } else
                return false;
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }


    @GetMapping("/admin")
    public String showAdminPortal(HttpSession session, Model model) {
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!logUser.getRole().equals(Role.ADMIN)) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "AdminPortal";
    }

    @GetMapping("/profile")
    public String showProfile(HttpSession session) {
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            return "redirect:/users/" + logUser.getId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping
    public String showAllUser(Model model, @ModelAttribute("filter") FilterUserDto filterUserDto, HttpSession session) {
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!logUser.getRole().equals(Role.ADMIN)) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        FilterOptionsUsers filterOptionsUsers = new FilterOptionsUsers(
                filterUserDto.getFirst_name(),
                filterUserDto.getLast_name(),
                filterUserDto.getUsername(),
                filterUserDto.getEmail(),
                filterUserDto.getSortBy(),
                filterUserDto.getSortOrder());
        List<User> userList = userService.get(filterOptionsUsers);
        model.addAttribute("users", userList);
        model.addAttribute("filter", filterUserDto);
        return "AllUsersView";
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        User singleUser = userService.getById(id);
        User logUser = authenticationHelper.tryGetUser(session);
        try {
                if (!(logUser.getRole().equals(Role.ADMIN) || logUser.getUsername().equals(singleUser.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }

        try {
            if (singleUser.getPhoneNumber() == null) {
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setPhone_number("No phone");
                singleUser.setPhoneNumber(phoneNumber);
            }
            model.addAttribute("user", singleUser);
            model.addAttribute("logUser",logUser);
            return "UserView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdatePage(@PathVariable int id, HttpSession session, Model model) {
        User currentUser = userService.getById(id);
        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(currentUser.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        UpdateUserDto dto = userMapper.toDto(currentUser);
        model.addAttribute("user", dto);
        return "UpdateUserView";
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UpdateUserDto userDto,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        User logUser = authenticationHelper.tryGetUser(session);
        User userToUpdate = userService.getById(id);
        try {
            if (!(logUser.getRole().equals(Role.ADMIN) || logUser.getUsername().equals(userToUpdate.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "UpdateUserView";
        }

        if(!userDto.getPassword().equals(userToUpdate.getPassword())){
            bindingResult.rejectValue("password","password_error","Password NOT match");
            return "UpdateUserView";
        }

        if (!userDto.getChangePassword().equals(userDto.getChangePasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "UpdateUserView";
        }

        try {
            User user = userMapper.fromDto(id,userDto);
            userService.update(user, logUser, id);
            return "redirect:/auth/login";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "UpdateUserView";
        }
    }


    @GetMapping("/{id}/admin")
    public String changeRoleToAdmin(@PathVariable int id, HttpSession session, Model model) {
        User currentUser = userService.getById(id);
        User logUser = authenticationHelper.tryGetUser(session);
        try {
            if (!(logUser.getRole().equals(Role.ADMIN))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.setUserToAdmin(currentUser, logUser);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/{id}/block")
    public String changeStatusBlock(@PathVariable int id, HttpSession session, Model model) {
        User currentUser = userService.getById(id);
        User logUser = authenticationHelper.tryGetUser(session);
        try {
            if (!(logUser.getRole().equals(Role.ADMIN) || logUser.getUsername().equals(currentUser.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.changeStatusToBlock(currentUser, logUser);
            if (logUser.getRole().equals(Role.ADMIN)) {
                return "redirect:/users";
            } else {
                return "redirect:/auth/login";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/active")
    public String changeStatusActive(@PathVariable int id, HttpSession session, Model model) {
        User currentUser = userService.getById(id);
        User logUser = authenticationHelper.tryGetUser(session);
        try {
            if (!logUser.getRole().equals(Role.ADMIN)) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.changeStatusToActive(currentUser, logUser);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }



}
