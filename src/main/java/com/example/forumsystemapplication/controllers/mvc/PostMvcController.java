package com.example.forumsystemapplication.controllers.mvc;

import com.example.forumsystemapplication.exeptions.AuthenticationFailureException;
import com.example.forumsystemapplication.exeptions.EntityDuplicateException;
import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.helpers.AuthenticationHelper;
import com.example.forumsystemapplication.helpers.PostMapper;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterPostDto;
import com.example.forumsystemapplication.models.Dto.PostDto;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.services.service.CommentService;
import com.example.forumsystemapplication.services.service.PostService;
import com.example.forumsystemapplication.services.service.TagService;
import com.example.forumsystemapplication.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/posts")
public class PostMvcController {

    private final PostService postService;
    private final UserService userService;
    private final PostMapper postMapper;
    private final CommentService commentService;
    private final TagService tagService;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public PostMvcController(PostService postService, UserService userService, PostMapper postMapper, CommentService commentService, TagService tagService, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.userService = userService;
        this.postMapper = postMapper;
        this.commentService = commentService;
        this.tagService = tagService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllPost(Model model, @ModelAttribute("filter") FilterPostDto filterDto) {

        FilterOptionsPosts filterOptionsPosts = new FilterOptionsPosts(
                filterDto.getTitle(),
                filterDto.getContent(),
                filterDto.getSortBy(),
                filterDto.getSortOrder()
        );

        List<Post> postList = postService.get(filterOptionsPosts);
        model.addAttribute("posts", postList);
        model.addAttribute("filter", filterDto);
        return "AllPostsView";
    }

    @GetMapping("/myPosts")
    public String showUserPosts(Model model,HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

            User user = authenticationHelper.tryGetUser(session);
             List<Post> posts = postService.getPostsByUser(user);
            model.addAttribute("posts", posts);
            return "MyPosts";
    }

    @GetMapping("/{id}")
    public String showSinglePostWithCommentsAndTags(@PathVariable int id, Model model,HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.getById(id);
            if(post.getStatus().equals(Status.BLOCK)){
                return "NotFoundView";
            }
            List<Comment> listOfComments = commentService.getCommentsByPost(post);
            List<Tag> listOfTags = tagService.getTagsByPost(post);
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("commentsCount",listOfComments.size());
            model.addAttribute("comments", listOfComments);
            model.addAttribute("tags", listOfTags);
            model.addAttribute("post", post);
            model.addAttribute("user", user);
            return "PostView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/like")
    public String UpdateLikePost(@PathVariable int id, Model model,HttpSession session){
        try {
            Post post = postService.getById(id);
            User user = authenticationHelper.tryGetUser(session);
            postService.updatePostLike(post,user);
            return "redirect:/posts/" + id;
        } catch (EntityNotFoundException e ){
            model.addAttribute("error",e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/new")
    public String showNewPostPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("post", new PostDto());
        return "CreatePostView";
    }


    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("post") PostDto postDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "CreatePostView";
        }

        try {
            Post post = postMapper.fromDto(postDto);
            postService.create(post, user);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_posts", e.getMessage());
            return "CreatePostView";
        }
    }

    @GetMapping("{id}/update")
    public String showUpdatePage(@PathVariable int id, Model model, HttpSession session) {
        Post post;
        try{
         post = postService.getById(id);

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(post.getCreateBy().getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

            post = postService.getById(id);
            PostDto postDto = postMapper.toDto(post);
            model.addAttribute("post", postDto);
            return "UpdatePostView";

    }

    @PostMapping("{id}/update")
    public String updatePost(@PathVariable int id,
                             @Valid @ModelAttribute("post") PostDto postDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        Post post = postMapper.fromDto(id, postDto);
        User user = post.getCreateBy();
        User logUser = authenticationHelper.tryGetUser(session);
        try {

            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(user.getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "UpdatePostView";
        }

        try {
            postService.update(post,logUser);
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_post", e.getMessage());
            return "UpdatePostView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deletePost(@PathVariable int id, Model model, HttpSession session) {
        Post post ;
        try {
            post = postService.getById(id);
        }catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        try {
            User logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin") || logUser.getUsername().equals(post.getCreateBy().getUsername()))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
            postService.delete(id, authenticationHelper.tryGetUser(session));
            return "redirect:/posts";
    }

}
