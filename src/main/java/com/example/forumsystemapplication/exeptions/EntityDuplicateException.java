package com.example.forumsystemapplication.exeptions;

public class EntityDuplicateException extends RuntimeException {

    public EntityDuplicateException(String name, String attribute, String value) {
        super(String.format(name, attribute, value));
    }
}
