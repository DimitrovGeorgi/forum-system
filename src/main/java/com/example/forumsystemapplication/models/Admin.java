package com.example.forumsystemapplication.models;

import javax.persistence.*;


@Entity
@Table(name = "admins")
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_id")
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String phone_number;


    public Admin(User user, String phone_number) {
        this.user = user;
        this.phone_number = phone_number;
    }

    public Admin() {

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
