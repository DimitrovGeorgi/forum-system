package com.example.forumsystemapplication.models.filters;

import java.util.Optional;

public class FilterOptionsUsers {

    private Optional<String> first_name;
    private Optional<String> last_name;
    private Optional<String> username;
    private Optional<String> email;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsUsers() {
        this(null, null, null, null, null, null);
    }

    public FilterOptionsUsers(String first_name, String last_name, String username, String email, String sortBy, String sortOrder) {
        this.first_name = Optional.ofNullable(first_name);
        this.last_name = Optional.ofNullable(last_name);
        this.username = Optional.ofNullable(username);
        this.email = Optional.ofNullable(email);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getFirst_name() {
        return first_name;
    }

    public Optional<String> getLast_name() {
        return last_name;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
