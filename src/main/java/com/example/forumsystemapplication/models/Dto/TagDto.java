package com.example.forumsystemapplication.models.Dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class TagDto {

    @NotNull
    @Size(min = 1, max = 50, message = "Tag must be between 1-50 symbols!")
    private String name;

    @Positive(message = "Post`s ID must be positive!")
    private int postId;

    public TagDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

}
