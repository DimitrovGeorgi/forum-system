package com.example.forumsystemapplication.models.Dto;

import com.example.forumsystemapplication.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class PostDto {
    @Size(min = 16, max = 64, message = "The title must be between 16 and 64 symbols!")
    private String title;
    @Size(min = 32, max = 8192, message = "The content must be between 32 and 8192 symbols!")
    private String content;

    public PostDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
