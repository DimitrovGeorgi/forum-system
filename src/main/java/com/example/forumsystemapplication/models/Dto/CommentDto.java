package com.example.forumsystemapplication.models.Dto;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CommentDto {

    @Size(min = 8, max = 1999, message = "Comment must be between 8-1999 symbols!")
    private String content;

    @Positive(message = "Post`s ID must be positive!")
    private int postId;

    public CommentDto() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
