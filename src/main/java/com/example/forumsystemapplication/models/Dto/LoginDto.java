package com.example.forumsystemapplication.models.Dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginDto {

    @Size(min = 4, max = 32, message = "Username must be between 4 and 32 symbols!")
    @NotEmpty
    private String username;
    @Size(min = 4, max = 32, message = "Password must be between 4 and 32 symbols!")
    @NotEmpty
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
