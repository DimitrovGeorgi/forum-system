package com.example.forumsystemapplication.models.Dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PhoneNumberDto {


    @Size( min = 8,max = 12, message = "Phone Number must be from 8 to 12")
    private  String phoneNumber;



    private int user_id;

    public PhoneNumberDto() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
