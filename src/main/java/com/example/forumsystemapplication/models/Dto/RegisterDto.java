package com.example.forumsystemapplication.models.Dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterDto extends LoginDto {
    @NotEmpty
    private String passwordConfirm;
    @NotEmpty
    @Size(min = 4, max = 32, message = "First name must be between 4 and 32 symbols!")
    private String firstName;
    @NotEmpty
    @Size(min = 4, max = 32, message = "Last name must be between 4 and 32 symbols!")
    private String LastName;
    @NotEmpty
    @Size(min = 4, max = 32, message = "Email must be between 4 and 32 symbols!")
    private String Email;

    private String photoUrl;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhotoUrl() { return photoUrl;}

    public void setPhotoUrl(String photoUrl) { this.photoUrl = photoUrl; }
}
