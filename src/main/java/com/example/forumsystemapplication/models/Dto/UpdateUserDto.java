package com.example.forumsystemapplication.models.Dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UpdateUserDto {

    @NotEmpty
    private String Password;

    @NotEmpty ( message = "Field cannot be empty!")
    @Size(min = 4, max = 32, message = "Your new password must be between 4 and 32 symbols!")
    private String changePassword;

    @NotEmpty ( message = "Field cannot be empty!")
    @Size(min = 4, max = 32, message = "Your new password must be between 4 and 32 symbols!")
    private String changePasswordConfirm;

    public UpdateUserDto() {
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getChangePasswordConfirm() {
        return changePasswordConfirm;
    }

    public void setChangePasswordConfirm(String changePasswordConfirm) {
        this.changePasswordConfirm = changePasswordConfirm;
    }

    public String getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(String changePassword) {
        this.changePassword = changePassword;
    }
}
