package com.example.forumsystemapplication.models.enums;

public enum Role {

    ADMIN, MODERATOR, USER;

    @Override
    public String toString() {
        switch (this) {
            case ADMIN:
                return "Admin";
            case MODERATOR:
                return "Moderator";
            case USER:
                return "User";
            default:
                return "";
        }
    }
}
