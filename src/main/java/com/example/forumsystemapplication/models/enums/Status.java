package com.example.forumsystemapplication.models.enums;

public enum Status {

    ACTIVE, BLOCK;


    @Override
    public String toString() {
        switch (this) {
            case BLOCK:
                return "Block";
            case ACTIVE:
                return "Active";
            default:
                return "";
        }
    }
}
