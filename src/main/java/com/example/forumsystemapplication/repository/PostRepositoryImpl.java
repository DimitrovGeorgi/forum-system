package com.example.forumsystemapplication.repository;


import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.repository.repo.PostRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.persistence.TypedQuery;
import java.util.*;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Post> get(FilterOptionsPosts filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptions.getTitle().ifPresent(value -> {
                filters.add("title like:title ");
                params.put("title", String.format("%%%s%%", value));
            });

            filterOptions.getContent().ifPresent(value -> {
                filters.add("content like:content ");
                params.put("content", String.format("%%%s%%", value));
            });


            StringBuilder query = new StringBuilder(" from Post ");


            if (!filters.isEmpty()) {
                query.append(" where ")
                        .append(String.join(" and ", filters));
            }
            query.append(generateOrderBy(filterOptions));
            System.out.println(query);
            Query<Post> postQuery = session.createQuery(query.toString(), Post.class);
            postQuery.setProperties(params);
            return postQuery.list();
        }
    }

    @Override
    public Post getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, id);
            if (post == null) {
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    @Override
    public Post getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post where title =: title", Post.class);
            query.setParameter("title", title);

            List<Post> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Post", "title", title);
            }
            return result.get(0);
        }
    }

    @Override
    public List<Post> getPostsByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("SELECT p FROM " +
                    "Post p WHERE p.createBy = :user", Post.class);
            query.setParameter("user", user);
            return query.list();
        }
    }

    @Override
    public void create(Post post) {
        try (Session session = sessionFactory.openSession()) {
            post.setStatus(Status.ACTIVE);
            session.save(post);
        }
    }

    @Override
    public void update(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            post.setStatus(Status.ACTIVE);
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateWhenDelete(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Post post = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            post.setStatus(Status.ACTIVE);
            session.delete(post);
            session.getTransaction().commit();
        }
    }

    private String generateOrderBy(FilterOptionsPosts filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "title":
                orderBy = "title";
                break;
            case "content":
                orderBy = "content";
                break;
            case "comment_count":
                orderBy = "comment_count";
                break;
            default:
                orderBy = "id";
        }

        orderBy = String.format(" order by %s", orderBy);

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }

}



