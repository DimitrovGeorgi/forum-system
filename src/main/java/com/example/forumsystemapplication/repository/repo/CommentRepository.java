package com.example.forumsystemapplication.repository.repo;

import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;

import java.util.List;

public interface CommentRepository {


    List<Comment> get();

    Comment getById(int id);

    void create(Comment comment);

    void update (Comment comment);

    void delete (int id);

    List<Comment> getCommentsByUser(User user);

    List<Comment> getCommentsByPost(Post post);

}
