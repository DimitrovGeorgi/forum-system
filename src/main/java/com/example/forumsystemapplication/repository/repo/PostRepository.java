package com.example.forumsystemapplication.repository.repo;

import com.example.forumsystemapplication.models.filters.FilterOptionsPosts;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;

import java.util.List;

public interface PostRepository {

    List<Post> get(FilterOptionsPosts filterOptions);

    Post getById(int id);

    Post getByTitle(String title);

    void create(Post post);


    void update(Post post);

    void updateWhenDelete(Post post);

    void delete(int id);

    List<Post> getPostsByUser(User user);




}
