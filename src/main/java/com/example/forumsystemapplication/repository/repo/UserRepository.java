package com.example.forumsystemapplication.repository.repo;

import com.example.forumsystemapplication.models.Admin;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;

import java.util.List;

public interface UserRepository {

    List<User> get(FilterOptionsUsers filterOptionsUsers);

    User getById(int id);

    void  create(User user);

    void update(User user);

    void delete(int id);

    User getByUsername(String username);

    void setUserToAdmin(User user);

    void changeStatusToBlock(User user);

    void changeStatusToActive(User user);

}
