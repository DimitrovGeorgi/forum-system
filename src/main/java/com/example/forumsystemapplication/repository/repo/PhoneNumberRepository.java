package com.example.forumsystemapplication.repository.repo;

import com.example.forumsystemapplication.models.PhoneNumber;

public interface PhoneNumberRepository {

    void create(PhoneNumber phoneNumber);

}
