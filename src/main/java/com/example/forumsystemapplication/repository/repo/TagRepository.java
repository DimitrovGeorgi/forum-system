package com.example.forumsystemapplication.repository.repo;

import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.Tag;
import com.example.forumsystemapplication.models.User;

import java.util.List;

public interface TagRepository {

    List<Tag> get();

    Tag getById(int id);

    List<Tag> getTagsByUser(User user);

    List<Tag> getTagsByPost(Post post);

    void create(Tag tag);

    void update (Tag tag);

    void delete (int id);
}
