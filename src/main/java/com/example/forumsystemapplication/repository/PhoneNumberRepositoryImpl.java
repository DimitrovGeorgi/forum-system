package com.example.forumsystemapplication.repository;

import com.example.forumsystemapplication.models.PhoneNumber;
import com.example.forumsystemapplication.repository.repo.PhoneNumberRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PhoneNumberRepositoryImpl implements PhoneNumberRepository {


    private final SessionFactory sessionFactory;
    @Autowired
    public PhoneNumberRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(PhoneNumber phoneNumber) {
        try(Session session = sessionFactory.openSession()){
            session.save(phoneNumber);
        }
    }
}
