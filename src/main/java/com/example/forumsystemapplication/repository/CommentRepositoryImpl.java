package com.example.forumsystemapplication.repository;

import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.models.Comment;
import com.example.forumsystemapplication.models.Post;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.repository.repo.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Comment> get() {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment", Comment.class);
            return query.list();
        }
    }

    @Override
    public Comment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Comment comment = session.get(Comment.class, id);
            if (comment == null) {
                throw new EntityNotFoundException("Comment", id);
            }
            return comment;
        }
    }

    @Override
    public List<Comment> getCommentsByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where createBy = :user", Comment.class);
            query.setParameter("user", user);
            return query.list();
        }
    }

    @Override
    public List<Comment> getCommentsByPost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where postOn = :post", Comment.class);
            query.setParameter("post", post);
            return query.list();
        }
    }

    @Override
    public void create(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            comment.setStatus(Status.ACTIVE);
            session.save(comment);
        }
    }

    @Override
    public void update(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(comment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Comment comment = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(comment);
            session.getTransaction().commit();
        }
    }

}
