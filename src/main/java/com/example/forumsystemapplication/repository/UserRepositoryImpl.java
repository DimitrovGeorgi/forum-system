package com.example.forumsystemapplication.repository;

import com.example.forumsystemapplication.exeptions.EntityNotFoundException;
import com.example.forumsystemapplication.models.User;
import com.example.forumsystemapplication.models.enums.Role;
import com.example.forumsystemapplication.models.enums.Status;
import com.example.forumsystemapplication.models.filters.FilterOptionsUsers;
import com.example.forumsystemapplication.repository.repo.UserRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.forumsystemapplication.utils.constants.ONLY_ADMIN;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> get(FilterOptionsUsers filterOptionsUsers) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptionsUsers.getFirst_name().ifPresent(value -> {
                filters.add("first_name like:first_name");
                params.put("first_name", String.format("%%%s%%", value));
            });

            filterOptionsUsers.getLast_name().ifPresent(value -> {
                filters.add("last_name like: last_name");
                params.put(" last_name", String.format("%%%s%%", value));
            });

            filterOptionsUsers.getUsername().ifPresent(value -> {
                filters.add("username like:username");
                params.put("username", String.format("%%%s%%", value));
            });

            filterOptionsUsers.getEmail().ifPresent(value -> {
                filters.add("email like:email");
                params.put("email", String.format("%%%s%%", value));
            });


            StringBuilder userQuery = new StringBuilder("from User ");
            if (!filters.isEmpty()) {
                userQuery.append(" where ")
                        .append(String.join(" and ", filters));
            }
            userQuery.append(generateOrder(filterOptionsUsers));

            Query<User> query = session.createQuery(userQuery.toString(), User.class);
            query.setProperties(params);
            return query.list();
        }
    }


    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username= :username", User.class);
            query = query.setParameter("username", username);
            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public void changeStatusToBlock(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setStatus(Status.BLOCK);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void changeStatusToActive(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setStatus(Status.ACTIVE);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void setUserToAdmin(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setRole(Role.ADMIN);
            session.update(user);
            session.getTransaction().commit();
        }
    }


    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setRole(Role.USER);
            user.setStatus(Status.ACTIVE);
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User user = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    private String generateOrder(FilterOptionsUsers filterOptionsUsers) {
        if (filterOptionsUsers.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (filterOptionsUsers.getSortBy().get()) {
            case "first_name":
                orderBy = "first_name";
                break;
            case "last_name":
                orderBy = "last_name";
                break;
            case "username":
                orderBy = "username";
                break;
            case "email":
                orderBy = "email";
                break;
            default:
                orderBy = "id";
        }

        orderBy = String.format(" order by %s", orderBy);

        if (filterOptionsUsers.getSortOrder().isPresent() &&
                filterOptionsUsers.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }

}
