package com.example.forumsystemapplication.utils;

public class constants {
    public static final String ONLY_ADMINS_OR_CREATORS_CAN_MODIFY = "Only admins or active creators can modify.";
    public static final String ONLY_FOR_ACTIVE_USERS = "Only for active users";
    public static final String ONLY_ADMIN = "Only Admins have that options";
    public static final String ONLY_CREATOR = "Only Creator have that options";

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String THIS_USERNAME_DOES_NOT_EXISTS = "Wrong username/password!";
    public static final String USER_BLOCK = "The USER is Blocked , you need to contact Admin";
    public static final String COMMENT_BLOCK = "The COMMENT is BLOCKED;";
    public static final String POST_BLOCK = "The POST is is BLOCKED;";





}
