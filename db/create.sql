create table users
(
    user_id      int auto_increment
        primary key,
    photo_url    varchar(320) default 'img/User_photo/default.jpg' null,
    first_name   varchar(32)                                       not null,
    last_name    varchar(32)                                       not null,
    email        varchar(255)                                      not null,
    username     varchar(32)                                       not null,
    password     varchar(255)                                      not null,
    role         varchar(20)                                       not null,
    status       varchar(20)                                       not null,
    phone_number varchar(20)                                       null,
    constraint email
        unique (email),
    constraint username
        unique (username)
);

create table phone_numbers
(
    phone_number_id int auto_increment
        primary key,
    phone_number    varchar(20) not null,
    user_id         int         null,
    constraint phone_number
        unique (phone_number),
    constraint phone_numbers_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id       int auto_increment
        primary key,
    user_id       int           not null,
    title         varchar(64)   not null,
    content       varchar(8192) not null,
    comment_count int           null,
    status        varchar(20)   null,
    constraint posts_ibfk_1
        foreign key (user_id) references users (user_id)
);

create table comments
(
    comment_id int auto_increment
        primary key,
    user_id    int           not null,
    post_id    int           not null,
    content    varchar(8192) not null,
    status     varchar(20)   not null,
    constraint comments_ibfk_1
        foreign key (user_id) references users (user_id),
    constraint comments_ibfk_2
        foreign key (post_id) references posts (post_id)
);

create index post_id
    on comments (post_id);

create index user_id
    on comments (user_id);

create table comments_users_likes
(
    comment_id int not null,
    user_id    int not null,
    constraint comments_users_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint comments_users_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create index user_id
    on posts (user_id);

create table posts_users_likes
(
    post_id int not null,
    user_id int not null,
    constraint posts_users_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_users_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table tags
(
    tag_id  int auto_increment
        primary key,
    user_id int           not null,
    post_id int           not null,
    name    varchar(8192) not null,
    constraint tags_ibfk_1
        foreign key (user_id) references users (user_id),
    constraint tags_ibfk_2
        foreign key (post_id) references posts (post_id)
);

