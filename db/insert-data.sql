

-- ---------------------------------------------------------------------------------------------------
INSERT INTO tags (user_id, post_id, name)
VALUES
    (1, 1, 'car'),
    (1, 1, 'sedan'),
    (1, 2, 'sports car'),
    (2, 1, 'automobile'),
    (2, 2, 'luxury car');



INSERT INTO Users (first_name, last_name, email, username, password,role,status,photo_url)
VALUES
    ('Georgi', 'Dimitrov', 'georgi@abv.bg', 'georgi', 'password', 'ADMIN' ,'ACTIVE','/assets/img/User_photo/1.jpg'),
    ('Kiril', 'Kostov', 'kirilKostov@abv.bg', 'batekiko', '123456','MODERATOR','ACTIVE','/assets/img/User_photo/2.jpg'),
    ('Radoslav', 'Popov', 'radoslavPopov@abv.bg', 'rado', '112233','USER','ACTIVE','/assets/img/User_photo/3.jpg');




INSERT INTO Posts (user_id, title, content,comment_count,status)
VALUES
    (1, 'Mercedes C-Class Tuning Guide', 'Here is a guide on how to tune your Mercedes C-Class for optimal performance.',2,'ACTIVE'),
    (2, 'BMW M3 vs. Mercedes AMG C63', 'A comparison between the BMW M3 and Mercedes AMG C63, which one is better?',1,'ACTIVE'),
    (3, 'Top 10 Supercars of All Time', 'Check out our list of the top 10 greatest supercars ever built.',1,'ACTIVE'),
    (2, 'Best car audio systems', 'Share your favorite car audio systems and sound setups!',0,'ACTIVE'),
    (1, 'Looking for recommendations: Affordable used cars', ' I am on a tight budget and looking for recommendations for affordable used cars.',0,'ACTIVE'),
    (3, 'Car detailing tips and tricks', 'Let us discuss the best techniques for car detailing and maintaining a pristine appearance.',0,'ACTIVE'),
    (1, 'Off-road adventure: Share your experiences', 'Have you recently gone on an off-road adventure? Share your experiences and recommendations!',0,'ACTIVE'),
    (2, 'The future of electric vehicles', 'Let us discuss the future of electric vehicles and the impact they will have on the automotive industry.',0,'ACTIVE'),
    (3, 'Classic car restoration projects', 'Share your classic car restoration projects, before-and-after pictures, and tips for fellow enthusiasts.',0,'ACTIVE'),
    (1, 'Dealing with car insurance claims', 'Share your experiences and advice on dealing with car insurance claims after an accident.',0,'ACTIVE'),
    (2, 'Tuning and upgrading turbocharged engines', 'Discuss the best tuning and upgrade options for turbocharged engines.',0,'ACTIVE'),
    (3, 'Car safety features: What is important to you?', 'Let discuss the essential car safety features that you consider when buying a new vehicle.',0,'ACTIVE'),
    (1, 'Formula 1: Predictions and discussions', 'Share your predictions and engage in discussions about the latest Formula 1 races and drivers.',0,'ACTIVE');


INSERT INTO Comments (user_id, post_id, content,status)
VALUES
    (2, 1, 'Great guide, thanks for sharing!','ACTIVE'),
    (3, 1, 'I tried these tuning tips on my C-Class and it made a noticeable difference.','ACTIVE'),
    (1, 2, 'Both cars are amazing, but I prefer the C63 for its sound and comfort.','ACTIVE'),
    (3, 3, 'I think the Bugatti Veyron should be on the list.','ACTIVE');


INSERT INTO forum_system.comments_users_likes (comment_id, user_id)
VALUES (1, 1);
INSERT INTO forum_system.comments_users_likes (comment_id, user_id)
VALUES (1, 2);
INSERT INTO forum_system.comments_users_likes (comment_id, user_id)
VALUES (2, 1);
INSERT INTO forum_system.comments_users_likes (comment_id, user_id)
VALUES (1, 3);

INSERT INTO forum_system.posts_users_likes (post_id, user_id)
VALUES (1, 1);
INSERT INTO forum_system.posts_users_likes (post_id, user_id)
VALUES (1, 2);
INSERT INTO forum_system.posts_users_likes (post_id, user_id)
VALUES (2, 1);
INSERT INTO forum_system.posts_users_likes (post_id, user_id)
VALUES (2, 2);
INSERT INTO forum_system.posts_users_likes (post_id, user_id)
VALUES (2, 3);


